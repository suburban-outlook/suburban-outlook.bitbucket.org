var config = require('./config.json'),
	fs = require('fs'),
	parse = require('csv-parse'),
	keen = require('keen-js');

var keenClient = new keen({
    projectId: config.keen.projectId,
    writeKey: config.keen.writeKey
});

var files = [
	'./data/couple_families_with_dependents/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_6.csv',
	'./data/couples_without_dependents/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_7.csv',
	'./data/group_households/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_1.csv',
	'./data/lone_person_households/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_2.csv',
	'./data/one_parent_family/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_4.csv',
	'./data/other_families/households_Year1_2013_mYear3_2043_OverlayID=0_themtype_PerChangeY1Y3_HouseholdTypeKey_8.csv',
];

files.map(function(file){

	var groupingType = file.split('/')[2];

	var input = fs.readFile(file, 'utf8', function(err, data){
		if (err) {
			throw err;
		}

		parse(data, {comment: '#'}, function(err, output){
			if (err) {
				throw err;
			}

			// As each row contains groups of values and percentages,
			// we need the ability to chunk our array into groups of 2.
			Array.prototype.chunk = function(chunkSize) {
			    var R = [];
			    for (var i=0; i<this.length; i+=chunkSize)
			        R.push(this.slice(i,i+chunkSize));
			    return R;
			}

			// column headings live in the first item on the array.
			var headings = output.shift();

			// shift off the grouping
			headings.shift();

			// shift off the area
			headings.shift();

			var headingsChunked = headings.chunk(2);
			var points = [];

			output.map(function(item){

				var grouping = item.shift();
				var area = item.shift();

				var chunked = item.chunk(2);
				chunked.map(function(chunk, index){

					/* We want something like the following:
					point: {
					    grouping: 'Couple families with dependents',
					    area: 'Wellington City',
					    year: 2013,
					    value: 36456,
					    percentage: 18.2
					}
					*/

					var heads = headingsChunked[index];
					var year = (heads[0]).split('.')[1];

					var point = {
						grouping: grouping,
						area: area,
						year: parseInt(year),
						value: parseInt(chunk[0].replace(/,/, '')),
						percentage: parseFloat(chunk[1]),
						timestamp: new Date().getTime(),
						keen: {timestamp: new Date(year).toISOString()}
					}

					points.push(point);

				});

			});

			keenClient.addEvents({'households': points}, function(err, res){
				if (err) {
					console.log('Error adding event to Keen.io', err);
			        return;
				} else {
					//console.log(res);
				}
			});

			console.log(groupingType + ': added ' + points.length + ' points');
//			console.log(points);

		});

	});

});
