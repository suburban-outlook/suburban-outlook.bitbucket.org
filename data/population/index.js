var config = require('./config.json'),
	fs = require('fs'),
	parse = require('csv-parse'),
	keen = require('keen-js');

var keenClient = new keen({
    projectId: config.keen.projectId,
    writeKey: config.keen.writeKey
});

var files = [
	'./age-0-15/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_0_CustomAgeTo_15.csv',
	'./age-16-25/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_16_CustomAgeTo_25.csv',
	'./age-26-35/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_26_CustomAgeTo_35.csv',
	'./age-36-45/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_36_CustomAgeTo_45.csv',
	'./age-46-55/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_46_CustomAgeTo_55.csv',
	'./age-56-65/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_56_CustomAgeTo_65.csv',
	'./age-66-75/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_66_CustomAgeTo_75.csv',
	'./age-76-85/population-age-structure-map_SexKey_0_mYear1_2013_mYear3_2043_CustomAgeFrom_76_CustomAgeTo_85.csv',
];

files.map(function(file){

	var ageRange = file.split('/')[1];

	var input = fs.readFile(file, 'utf8', function(err, data){
		if (err) {
			throw err;
		}

		parse(data, {comment: '#'}, function(err, output){
			if (err) {
				throw err;
			}

			// As each row contains groups of values and percentages,
			// we need the ability to chunk our array into groups of 2.
			Array.prototype.chunk = function(chunkSize) {
			    var R = [];
			    for (var i=0; i<this.length; i+=chunkSize)
			        R.push(this.slice(i,i+chunkSize));
			    return R;
			}

			// column headings live in the first item on the array.
			var headings = output.shift();

			// shift off the age
			headings.shift();

			// shift off the area
			headings.shift();

			var headingsChunked = headings.chunk(2);
			var points = [];

			output.map(function(item){

				var cohort = item.shift();
				var area = item.shift();

				var chunked = item.chunk(2);
				chunked.map(function(chunk, index){

					/* We want something like the following:
					point: {
					    cohort: '0-15',
					    area: 'Wellington City',
					    year: 2013,
					    value: 36456,
					    percentage: 18.2
					}
					*/

					var heads = headingsChunked[index];
					var year = (heads[0]).split('.')[1];

					var point = {
						cohort: cohort,
						area: area,
						year: parseInt(year),
						value: parseInt(chunk[0].replace(/,/, '')),
						percentage: parseFloat(chunk[1]),
						timestamp: new Date().getTime(),
						keen: {timestamp: new Date(year).toISOString()}
					}

					points.push(point);

				});

			});

			keenClient.addEvents({'population': points}, function(err, res){
				if (err) {
					console.log('Error adding event to Keen.io', err);
			        return;
				} else {
					//console.log(res);
				}
			});

			console.log(ageRange + ': added ' + points.length + ' points');
			//console.log(points);

		});

	});

});
