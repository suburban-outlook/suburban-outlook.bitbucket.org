var config = require('./config.json'),
	fs = require('fs'),
	parse = require('csv-parse'),
	keen = require('keen-js');

var keenClient = new keen({
    projectId: config.keen.projectId,
    writeKey: config.keen.writeKey
});

var files = [
	'./Dwellings-Development\ 2013-2043.csv',
];

files.map(function(file){

	var input = fs.readFile(file, 'utf8', function(err, data){
		if (err) {
			throw err;
		}

		parse(data, {comment: '#'}, function(err, output){
			if (err) {
				throw err;
			}

			// column headings live in the first item on the array.
			var headings = output.shift();

			// shift off the area
			headings.shift();

			var points = [];

			output.map(function(item){

				var area = item.shift();

				item.map(function(chunk, index){

					/* We want something like the following:
					point: {
					    cohort: '0-15',
					    area: 'Wellington City',
					    year: 2013,
					    value: 36456
					}
					*/

					var heads = headings[index];
					var year = heads;

					var point = {
						area: area,
						year: parseInt(year),
						value: parseInt(chunk.replace(/,/, '')),
						timestamp: new Date().getTime(),
						keen: {timestamp: new Date(year).toISOString()}
					}

					points.push(point);

				});

			});

			keenClient.addEvents({'dwellings': points}, function(err, res){
				if (err) {
					console.log('Error adding event to Keen.io', err);
			        return;
				} else {
					//console.log(res);
				}
			});

			console.log('added ' + points.length + ' points');
//			console.log(points);

		});

	});

});
