# Suburban Outlook

This is the project repository for [Suburban Outlook](https://hackerspace.govhack.org/content/suburban-outlook), a project for [GovHack 2015](http://govhack.org.nz/).

## Introduction

> Picking a suburb can be a stressful decision. Is it the best place to raise a family, does it have good transport connections, is it safe? But what a suburb looks like today can change over time, and the neighbourhood you choose to put down roots in today could look very different in 10, 20, 30 years. Using city council data and population projections, we will help Wellingtonians make informed decisions, helping them have a glimpse into the future at what their suburb is likely to look like. What age will the people be, how will the buildings change, how affordable will it be?

As we're using [bitbucket pages](http://pages.bitbucket.org/), this project can be seen at: https://suburban-outlook.bitbucket.io/ (previously, at http://suburban-outlook.bitbucket.org/)

## Technology

This project is a single-page application, which relies on [AngularJS](https://angularjs.org/), [Pure.css](http://purecss.io/) and [keen.io](https://keen.io)

## Data service

We use [keen.io](https://keen.io/) as our data provider, and there's some simple node scripts in the `data` directory which demonstrates how we upload data to keen using [keen's API](https://keen.io/docs/api/).

Once our data has been pumped into keen, we can then query it using keen's API. In our application, then main aggregate query we use is [keen's sum](https://keen.io/docs/api/#sum) query, whereby we sum the values (population, number of dwellings etc) by area (i.e. suburb) for a given timeframe:

    function sum(client, collection, queryDate) {
        return {
            "method" : "POST",
            "url" : "https://api.keen.io/3.0/projects/" + client.projectId()+ "/queries/sum",
            headers:{},
            params : {
                "api_key" : client.readKey(),
                "event_collection" : collection,
                "target_property" : "value",
                "group_by": "area",
                "timeframe" : {
                    "start" : queryDate + "-01-01T00:00:00.000+00:00",
                    "end" : queryDate + "-12-31T00:00:00.000+00:00"
                }
            },
            data: {
                "filters": [{
                    "property_name" : "year",
                    "operator" : "eq",
                    "property_value" : queryDate
                }],
                "maxAge": 43200
            }
        };
    }

## Charting

We also use [keen's data visualization service](https://github.com/keen/keen-js/blob/master/docs/visualization.md) to execute queries and graph the results.

    // ----------------------------------------
    // Area Chart: Population by Age (%), 2013-2043
    // ----------------------------------------
    var population_2013_2043 = new Keen.Query("maximum", {
            eventCollection: "population",
            targetProperty: "percentage",
            interval: "yearly",
            timeframe: {
              start: "2013-01-01T00:00:00.000Z",
              end: "2044-01-01T00:00:00.000Z"
            },
            groupBy: "cohort",
            filters: [
                {
                  property_name: "area",
                  operator: "eq",
                  property_value: suburb.name
                }
            ],
            maxAge: 43200 // 24 hours
        });

    $scope.client.draw(population_2013_2043, document.getElementById("chart-4"), {
        chartType: "areachart",
        title: "Population by Age (%), 2013-2043",
        height: 500,
        width: "auto",
        chartOptions: {
            chartArea: {
                height: "80%",
                left: "15%",
                top: "10%",
                width: "70%"
            },
            //legend: {position: 'top'},
            isStacked: false
        }
    });

By using the `maxAge` parameter, we are instructing keen to serve [a cached version](https://keen.io/blog/119953161786/speed-up-dashboards-with-query-caching) of the graph.
