(function(){

	var app = angular.module('suburbanOutlook', []);

	app.factory('AngKeen', function($http) {
	    var AngKeen = {
	        fetch: function(req) {
	            return $http(req).then(function(response) {
	                return response.data;
	            });
	        }
	    };

	    return AngKeen;
	});

	app.controller('SuburbController', function($scope, $http, $log, $q, AngKeen) {

	    $scope.data = AngKeen;

		$scope.client = new Keen({
		  projectId: "5597501996773d0859920907", // String (required always)
		  readKey: "926fb455001efad8c52df7499aff959311d6ce3d26e229576cbdd6775a461bb70c3b83d4333f99b3ac6a8f6e2e47b2aa453ce17666afeb82cb662b3db71ea59e852b9fe2813949148a346ff2c100e6658e9315cb83e021548c90a08a9cefde78bf53a39b8084be356b98f8dd9cc1d4f7"      // String (required for querying data)
		});

		$scope.suburbs = [
		    {name: 'Wellington City'},
		    {name: 'Aro Valley - Highbury'},
		    {name: 'Berhampore'},
		    {name: 'Brooklyn'},
		    {name: 'Churton Park - Glenside'},
		    {name: 'Grenada Village - Paparangi - Woodridge - Horokiwi'},
		    {name: 'Hataitai'},
		    {name: 'Island Bay - Owhiro Bay'},
		    {name: 'Johnsonville'},
		    {name: 'Kaiwharawhara - Khandallah - Broadmeadows'},
		    {name: 'Karori'},
		    {name: 'Kelburn'},
		    {name: 'Kilbirnie - Rongotai - Moa Point'},
		    {name: 'Kingston - Mornington - Vogeltown'},
		    {name: 'Lyall Bay'},
		    {name: 'Miramar - Maupuia'},
		    {name: 'Mt Cook'},
		    {name: 'Mt Victoria'},
		    {name: 'Newlands - Ngauranga'},
		    {name: 'Newtown'},
		    {name: 'Ngaio - Crofton Downs'},
		    {name: 'Northland - Wilton'},
		    {name: 'Ohariu - Makara - Makara Beach'},
		    {name: 'Roseneath - Oriental Bay'},
		    {name: 'Seatoun - Karaka Bays - Breaker Bay'},
		    {name: 'Southgate - Houghton Bay - Melrose'},
		    {name: 'Strathmore Park'},
		    {name: 'Tawa - Grenada North - Takapu Valley'},
		    {name: 'Te Aro'},
		    {name: 'Thorndon - Pipitea'},
		    {name: 'Wellington Central'},
		];

	    // Set the initial suburb
	    $scope.suburb = $scope.suburbs[0];

	    $scope.now = new Date().getFullYear(); // current year
	    this.now = $scope.now;
	    $scope.futureDate = 2043;

	    $scope.fetchData = function(type, now, future) {

	        var deferNow = $q.defer();

	        AngKeen.fetch(sum($scope.client, type, now))
	            .then(function(data) {
	                if (data && data.result && data.result.length > 0) {

	                	data.result.forEach(function(item){
	                		$scope[type] = $scope[type] || {};
	                		var area = $scope[type][item.area] || {};
	                		area.now = item.result;
		                    $scope[type][item.area] = area;
	                	});

	                }
	                deferNow.resolve(type + ' fetched!');
	            }
	        );

	        var deferFuture = $q.defer();

	        AngKeen.fetch(sum($scope.client, type, future))
	            .then(function(data) {
	                if (data && data.result && data.result.length > 0) {

	                	data.result.forEach(function(item){
	                		$scope[type] = $scope[type] || {};
	                		var area = $scope[type][item.area] || {};
	                		area.future = item.result;
		                    $scope[type][item.area] = area;
	                	});

	                }
	                deferFuture.resolve(type + ' fetched!');
	            }
	        );

	        return $q.all([deferNow.promise, deferFuture.promise]);
	    }

	    $scope.graphs = function(suburb) {
	        // ----------------------------------------
	        // *********** POPULATION GRAPHS **********
	        // ----------------------------------------

	        // ----------------------------------------
	        // Area Chart: Number of Residents 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "population",
	                targetProperty: "value",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-3"), {
	            chartType: "areachart",
	            title: "Total Population, 2013-2043",
	            height: 500,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "15%",
	                    top: "5%",
	                    width: "70%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Population by Age (%), 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "population",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                groupBy: "cohort",
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-4"), {
	            chartType: "areachart",
	            title: "Population by Age (%), 2013-2043",
	            height: 500,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "80%",
	                    left: "15%",
	                    top: "10%",
	                    width: "70%"
	                },
					//legend: {position: 'top'},
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Population by Age, 0-15 Year-old Cohort (%), 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "population",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    },
	                    {
	                      property_name: "cohort",
	                      operator: "eq",
	                      property_value: "0-15"
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-1"), {
	            chartType: "areachart",
	            title: "Population by Age, 0-15 Year-old Cohort (%), 2013-2043",
	            height: 250,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "10%",
	                    top: "10%",
	                    width: "80%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Population by Age, 76-85 Year-old Cohort (%), 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "population",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    },
	                    {
	                      property_name: "cohort",
	                      operator: "eq",
	                      property_value: "76-85"
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-2"), {
	            chartType: "areachart",
	            title: "Population by Age, 76-85 Year-old Cohort (%), 2013-2043",
	            height: 250,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "10%",
	                    top: "10%",
	                    width: "80%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // *********** DWELLINGS GRAPHS ***********
	        // ----------------------------------------

	        // ----------------------------------------
	        // Area Chart: Number of Dwellings, 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "dwellings",
	                targetProperty: "value",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-8"), {
	            chartType: "areachart",
	            title: "Number of Dwellings, 2013-2043",
	            height: 500,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "15%",
	                    top: "5%",
	                    width: "70%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Households (%), 2013-2043
	        // ----------------------------------------
	        var households_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "households",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                groupBy: "grouping",
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(households_2013_2043, document.getElementById("chart-7"), {
	            chartType: "areachart",
	            title: "Households (%), 2013-2043",
	            height: 500,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "80%",
	                    left: "15%",
	                    top: "10%",
	                    width: "70%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Households, Couple families with dependents (%), 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "households",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    },
	                    {
	                      property_name: "grouping",
	                      operator: "eq",
	                      property_value: "Couple families with dependents"
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-5"), {
	            chartType: "areachart",
	            title: "Households, Couple families with dependents (%), 2013-2043",
	            height: 250,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "10%",
	                    top: "10%",
	                    width: "80%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Area Chart: Households, Lone person households, (%), 2013-2043
	        // ----------------------------------------
	        var population_2013_2043 = new Keen.Query("maximum", {
	                eventCollection: "households",
	                targetProperty: "percentage",
	                interval: "yearly",
	                timeframe: {
	                  start: "2013-01-01T00:00:00.000Z",
	                  end: "2044-01-01T00:00:00.000Z"
	                },
	                filters: [
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    },
	                    {
	                      property_name: "grouping",
	                      operator: "eq",
	                      property_value: "Lone person households"
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(population_2013_2043, document.getElementById("chart-6"), {
	            chartType: "areachart",
	            title: "Households, Lone person households (%), 2013-2043",
	            height: 250,
	            width: "auto",
	            chartOptions: {
	                chartArea: {
	                    height: "85%",
	                    left: "10%",
	                    top: "10%",
	                    width: "80%"
	                },
	                isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Bar Chart: Households (%), now
	        // ----------------------------------------
	        var households_now = new Keen.Query("sum", {
	                eventCollection: "households",
	                targetProperty: "percentage",
	                groupBy: "grouping",
	                filters: [
	                    {
	                      property_name: "year",
	                      operator: "eq",
	                      property_value: $scope.now
	                    },
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(households_now, document.getElementById("chart-2a"), {
	            chartType: "barchart",
	            title: "Households (%), "+$scope.now,
	            height: 357,
	            width: "auto",
	            chartOptions: {
	              chartArea: {
	                height: "85%",
	                left: "35%",
	                top: "5%",
	                width: "80%"
	              },
	              legend: { position: "none" },
	              isStacked: false
	            }
	        });

	        // ----------------------------------------
	        // Bar Chart: Households (%), future
	        // ----------------------------------------
	        var households_future = new Keen.Query("sum", {
	                eventCollection: "households",
	                targetProperty: "percentage",
	                groupBy: "grouping",
	                filters: [
	                    {
	                      property_name: "year",
	                      operator: "eq",
	                      property_value: $scope.futureDate
	                    },
	                    {
	                      property_name: "area",
	                      operator: "eq",
	                      property_value: suburb.name
	                    }
	                ],
	                maxAge: 43200 // 24 hours
	            });

	        $scope.client.draw(households_future, document.getElementById("chart-2b"), {
	            chartType: "barchart",
	            title: "Households (%), "+$scope.futureDate,
	            height: 357,
	            width: "auto",
	            chartOptions: {
	              chartArea: {
	                height: "85%",
	                left: "35%",
	                top: "5%",
	                width: "80%"
	              },
	              legend: { position: "none" },
	              isStacked: false
	            }
	        });

	    }

	    $scope.update = function() {

	        // Only fetch the population data if we haven't already cached it.
	        var deferPopulation = $q.defer();

	        if (!$scope.population) {
	            $scope.fetchData('population', $scope.now, $scope.futureDate).then(function(){
	                deferPopulation.resolve('population fetched!');
	            });
	        } else {
	                deferPopulation.resolve('population fetched from cache!');
	        }

	        // Only fetch the dwellings data if we haven't already cached it.
	        var deferDwellings = $q.defer();

	        if (!$scope.dwellings) {
	            $scope.fetchData('dwellings', $scope.now, $scope.futureDate).then(function(){
	                deferDwellings.resolve('dwellings fetched!');
	            });
	        } else {
	                deferDwellings.resolve('dwellings fetched from cache!');
	        }

	        return $q.all([deferPopulation.promise, deferDwellings.promise]);
	    }

	    $scope.changeSuburb = function(suburb) {
	        $scope.fetched = false;

	        return $scope.update().then(function(){
				$('.charts').removeClass('hidden');
	            $scope.graphs(suburb);
	            $scope.fetched = true;
	        });
	    }

	    // Fetch data for the initial suburb
	    $scope.changeSuburb($scope.suburb);

	    $scope.range = function(min, max, step){
	        step = step || 1;
	        var input = [];
	        for (var i = min; i <= max; i += step) input.push(i);
	        return input;
	    };

	    this.percentageIncrease = function(n1, n2) {
	        return (((n2-n1)/n1)*100).toFixed(2);
	    }

	    // this.scrollToPopulation = function() {
	    //     $('html, body').animate({
	    //         scrollTop: $("a[name='population']").offset().top
	    //     }, 2000);

	    //     //$("#populationOutput").toggle();
	    // }

	    // this.scrollToDwellings = function() {
	    //     $('html, body').animate({
	    //         scrollTop: $("a[name='dwellings']").offset().top
	    //     }, 2000);

	    //     //$("#dwellingOutput").toggle();
	    // }

	});

	function sum(client, collection, queryDate) {
	    return {
	        "method" : "POST",
	        "url" : "https://api.keen.io/3.0/projects/" + client.projectId()+ "/queries/sum",
	        headers:{},
	        params : {
	            "api_key" : client.readKey(),
	            "event_collection" : collection,
	            "target_property" : "value",
	        	"group_by": "area",
	            "timeframe" : {
	            	"start" : queryDate + "-01-01T00:00:00.000+00:00",
	            	"end" : queryDate + "-12-31T00:00:00.000+00:00"
	            }
	        },
	        data: {
		        "filters": [{
	             	"property_name" : "year",
	                "operator" : "eq",
	                "property_value" : queryDate
	            }],
		        "maxAge": 43200
	        }
	    };
	}
})();
